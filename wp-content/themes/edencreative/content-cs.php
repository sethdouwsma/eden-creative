<?php
/**
 * The default template for displaying case study content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row padding">
		<div class="project large-8 medium-12">
			<h3>The Project</h3>
			<hr>
			<?php the_field('the_project'); ?>
		</div><!-- 
		--><div class="cs_logo large-4 medium-12"><img src="<?php the_field('cs_logo'); ?>" alt="<?php the_title(); ?>"></div>
	</div>
	
	<div class="row">
		<?php
			$rows = get_field('secondary_images' ); // get all the rows
			$first_row = $rows[0]; // get the first row
			$second_row = $rows[1]; // get the first row
			$third_row = $rows[2]; // get the first row
		?>
		<div class="sec_image sec_image_1 large-6 medium-12" style="background-image: url(<?php echo $first_row['image']; ?>);"></div><!-- 
		--><div class="goal large-6 medium-12">
			<h3>The Goal</h3>
			<hr>
			<?php the_field('the_goal'); ?>
		</div>
	</div>
	
	<div class="row padding">
		<div class="desktop_image"><img src="<?php the_field('desktop_image'); ?>" alt="<?php the_title(); ?>"></div>
	</div>
	
	<div class="row">
		<div class="sec_image sec_image_2 large-6 medium-12" style="background-image: url(<?php echo $second_row['image']; ?>);"></div><!-- 
		--><div class="sec_image sec_image_3 large-6 medium-12" style="background-image: url(<?php echo $third_row['image']; ?>);"></div>
	</div>
	
	<div class="row padding">
		<div class="responsive_image"><img src="<?php the_field('responsive_image'); ?>" alt="<?php the_title(); ?>"></div>
	</div>
	
	<div class="row padding">
		<div class="outcome large-6 medium-12">
			<h3>The Outcome</h3>
			<hr>
			<?php the_field('the_outcome'); ?>
		</div><!-- 
		--><div class="testimonial large-6 medium-12">
			<p class="h4"><?php the_field('testimonial'); ?></p>
			<p class="h5"><?php the_field('testimonial_name'); ?></p>
		</div>
	</div>
		
	<div class="row padding cs_link">
		<a href="http://<?php the_field('link'); ?>" target="_blank" class="button-red-stroked">Visit <?php the_field('link'); ?></a>
	</div>
	
</article>
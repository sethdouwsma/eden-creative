<?php get_header(); ?>
	
	
	<?php if ( in_category( 'the-why-initiative' ) ) : ?>
		<div class="post_navigation clearfix">
			<div class="post_nav_item back"><a href="<?php echo get_category_link(18); ?>">Back</a></div>
			<div class="post_nav_item previous"><?php previous_post_link('%link', TRUE); ?></div>
			<div class="post_nav_item next"><?php next_post_link('%link', TRUE); ?></div>
		</div><!-- end post_navigation -->
	<?php else : ?>
		<div class="post_navigation clearfix">
			<div class="post_nav_item back"><a href="<?php echo get_permalink(375); ?>">Back</a></div>
			<div class="post_nav_item previous"><?php previous_post_link('%link'); ?></div>
			<div class="post_nav_item next"><?php next_post_link('%link'); ?></div>
		</div><!-- end post_navigation -->
	<?php endif; ?>

	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0];
	?>

	<?php if ( in_category( 'the-why-initiative' ) ) : ?>
		<header class="twi_header" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/twi_header.jpg);">
			<img src="<?php echo get_template_directory_uri(); ?>/images/twi_logo_white.svg" />
		</header><!-- end header -->
	<?php else : ?>
		<header style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/journal_bg.jpg);">
		</header><!-- end header -->
	<?php endif; ?>

	<div class="content section container">
				
		<?php if ( have_posts() ) : ?>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		
		<?php if ( in_category( 'the-why-initiative' ) ) : ?>
			<ul class="journal_wrap stacked">
				<?php 
					$current = get_the_ID();
					
				 	$args = array(
				        'posts_per_page' => 3,
				        'post__not_in'	=> array($current),
						'category_name'	=> 'the-why-initiative',
					    'orderby'	=> 'date',
						'order'		=> 'DESC' //  Newst To Oldest
				    );
					query_posts( $args ); 
				?>
				<?php if ( have_posts() ) : ?>
					<h3>More From The Why Initiative</h3>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'index_excerpt' ); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</ul>
		<?php else : ?>
			<ul class="journal_wrap stacked">
				<?php 
					$fname = get_the_author();
					$author = $post->post_author;
					$current = get_the_ID();
					
				 	$args = array(
				        'posts_per_page' => 3,
				        'post__not_in'	=> array($current),
				        'author'	=> $author,
					    'orderby'	=> 'date',
						'order'		=> 'DESC' //  Newst To Oldest
				    );
					query_posts( $args ); 
				?>
				<?php if ( have_posts() ) : ?>
					<h3>Articles by <?php echo strtok($fname, " "); ?></h3>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'index_excerpt' ); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</ul>
		
		<?php endif; ?>
		
	</div><!-- end .content -->

<?php get_footer(); ?>
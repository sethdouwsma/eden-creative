<?php 
// Main Category Template
?>

<?php get_header(); ?>

	<?php if ( is_category( 'the-why-initiative' ) ) : ?>
		<header class="twi_header" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/twi_header.jpg);">
			<img src="<?php echo get_template_directory_uri(); ?>/images/twi_logo_white.svg" />
		</header><!-- end header -->
	<?php else : ?>
		<header>
			<div class="container">
				<h1>Our <span>Two</span> Cents</h1>
				<h3>Articles Posted in "<?php single_cat_title(); ?>"</h3>
			</div>	
		</header><!-- end header -->
	<?php endif; ?>

	<div class="content section container">
		<ul class="journal_wrap stacked">
			<?php
				$categories = get_the_category();
				$cat = $categories[0]->cat_ID;
			 	$args = array(
		            'posts_per_page' => -1,
		            'cat'		=> $cat,
				    'orderby'	=> 'date',
					'order'		=> 'DESC' //  Newst To Oldest
		        );
				query_posts( $args ); 
			?>
		  	<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'index_excerpt' ); ?>
				<?php endwhile; ?>
		  	<?php endif; ?>
			<?php wp_reset_query(); ?>
		</ul>		
	</div><!-- end content -->

<?php get_footer(); ?>
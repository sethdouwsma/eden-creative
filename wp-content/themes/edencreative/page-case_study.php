<?php 
/* 
Template Name: Case Studies
*/
?>

<?php get_header(); ?>
	
	<header>
		<div class="container">
			<h1>Our <span>Recent</span> Work</h1>
		</div>	
	</header><!-- end header -->

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
				<div class="intro section container">
					<h3><?php the_title(); ?></h3>
					<p>Each project is an opportunity. It’s an opportunity for us to not only create wonderful solutions, but to create and execute a plan to help others. Here at Eden, that’s what we’re all about.</p>
				</div>
				<ul class="cs_buckets">
					<?php
					 	$args = array(
				            'posts_per_page' => -1,
				            'post_type'	=> 'case_study',
						    'orderby'	=> 'date',
							'order'		=> 'DESC' //  Newst To Oldest
				        );
						query_posts( $args ); 
					?>
				  	<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'cs_excerpt' ); ?>
						<?php endwhile; ?>
				  	<?php endif; ?>
					<?php wp_reset_query(); ?>
				</ul>		
			</div><!-- end content -->
	
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>

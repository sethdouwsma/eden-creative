<?php get_header(); ?>
	
	<div class="post_navigation clearfix">
		<div class="post_nav_item back"><a href="<?php echo get_permalink(7); ?>">Back</a></div>
		<div class="post_nav_item previous"><?php previous_post_link('%link'); ?></div>
		<div class="post_nav_item next"><?php next_post_link('%link'); ?></div>
	</div><!-- end post_navigation -->

	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0];
	?>

	<header style="background-image: url(<?php echo $thumb_url; ?>);">
		<div class="overlay"></div>
		<div class="container">
			<h1><?php the_title(); ?></h1>
			<h4><?php the_field('location'); ?></h4>
		</div>	
	</header><!-- end header -->

	<div class="content">
				
		<?php if ( have_posts() ) : ?>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'cs' ); ?>
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>
		
		<div class="cta_cs">
			<p><a href="<?php echo get_permalink(11); ?>">Let's Work Together &#8594;</a></p>
		</div>
		
	</div><!-- end .content -->

<?php get_footer(); ?>
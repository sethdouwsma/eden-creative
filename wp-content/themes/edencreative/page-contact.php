<?php 
/* 
Template Name: Contact
*/
?>

<?php get_header(); ?>
	
	<header>
		<div class="container">
			<h1>Let’s <span>Meet</span> Each Other</h1>
		</div>	
	</header><!-- end header -->

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content section container">
				<div class="intro">
					<h2>We would love to hear from you.</h2>
					<p>As much as we love creating and developing, we enjoy building relationships just as much, if not more. Shoot us an email, give us a call, send a carrier pigeon. Let’s create something beautiful.</p>
					<hr>
					<ul class="social_list contact_social">
						<li class="twitter"><a href="http://twitter.com/edencreativeco" target="_blank">Twitter</a></li>
						<li class="dribbble"><a href="http://dribbble.com/edencreative" target="_blank">Dribbble</a></li>
						<li class="instagram"><a href="http://instagram.com/edencreativeco" target="_blank">Instagram</a></li>
						<li class="facebook"><a href="http://facebook.com/edencreativeco" target="_blank">Facebook</a></li>
						<li class="email"><a href="mailto:hello@edencreative.co">Email</a></li>
					</ul>
				</div>
					
				<div class="contact_form">
					<?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 1 ); } ?>
				</div>				
			</div><!-- end content -->
	
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>

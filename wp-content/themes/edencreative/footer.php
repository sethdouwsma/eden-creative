<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 */
?>
				
			<footer>
				<div class="container clearfix">
					<p class="footer_contact">574.849.9040 <span>|</span> 310 Luelde St. South Bend, IN 46614 <span>|</span> <a href="mailto:hello@edencreative.co">hello@edencreative.co</a></p>
					<p class="button"><a href="<?php echo get_page_link(11) ?>" class="button-gold-stroked">Contact Us</a></p>
					<div class="main_nav_footer">
						<ul class="main_nav_social">
							<li class="twitter"><a href="http://twitter.com/edencreativeco" target="_blank">Twitter</a></li>
							<li class="dribbble"><a href="http://dribbble.com/edencreative" target="_blank">Dribbble</a></li>
							<li class="instagram"><a href="http://instagram.com/edencreativeco" target="_blank">Instagram</a></li>
							<li class="facebook"><a href="http://facebook.com/edencreativeco" target="_blank">Facebook</a></li>
						</ul>
						<div class="copyright">
							<p>Made in the Midwest</p>
							<p>&copy; <?php echo date('Y'); ?> Eden Creative, LLC</p>
						</div>
					</div>
				</div>
			</footer>
	
		</div><!-- end #main -->
	
	</div><!-- #page_container -->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/tweetie.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.singlePageNav.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.glide.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
	<!-- <script type="text/javascript" src="js/scripts.min.js"></script> -->
	
	<script type="text/javascript">
	    $('#tweets').twittie({
	        username: 'edencreativeco',
	        dateFormat: '%b. %d, %Y',
	        template: '{{tweet}} <div class="date">{{date}}</div>',
	        count: 1,
	        hideReplies: true
	    });
	</script>
	
	<?php wp_footer(); ?>
	
</body>
</html>

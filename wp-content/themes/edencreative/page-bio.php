<?php 
/* 
Template Name: Bios
*/
?>

<?php get_header(); ?>
	
	<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$thumb_url = $thumb_url_array[0];
	?>
	
	<header style="background-image: url(<?php echo $thumb_url; ?>);">
		<div class="container">
			<h1><?php the_title(); ?></h1>
			<h4><?php the_field('title'); ?></h4>
		</div>	
	</header><!-- end header -->

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content container">
				<div class="intro section">
					<div class="quote">
						<h3><?php the_field('quote'); ?></h3>
						<hr>
						<ul class="social_list bio_social">
							<li class="twitter"><a href="<?php the_field('twitter'); ?>" target="_blank">Twitter</a></li>
							<li class="dribbble"><a href="<?php the_field('dribbble'); ?>" target="_blank">Dribbble</a></li>
							<li class="linkedin"><a href="<?php the_field('linkedin'); ?>" target="_blank">LinkedIn</a></li>
							<li class="spotify"><a href="<?php the_field('spotify'); ?>" target="_blank">Spotify</a></li>
						</ul>
					</div>
				</div>
				
				<div class="bio_content">
					<?php the_content(); ?>
				</div>
				
				<ul class="section journal_wrap stacked clearfix">
					<?php 
						$fname = get_the_title();
						$author = "";
						if (is_page(659)) {
							$author = 1;
						} elseif (is_page(661)) {
							$author = 2;
						}
					?> 
					<h3>Articles by <?php echo strtok($fname, " "); ?></h3>
					<?php
					 	$args = array(
					        'posts_per_page' => 3,
					        'author'	=> $author,
						    'orderby'	=> 'date',
							'order'		=> 'DESC' //  Newst To Oldest
					    );
						query_posts( $args ); 
					?>
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content', 'index_excerpt' ); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>
				</ul>
						
			</div><!-- end content -->
	
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>

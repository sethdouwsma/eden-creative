<?php
/**
 * The default template for displaying journal buckets
 *
 */
?>

<li class="journal_bucket large-4">
	<h3 class="entry_title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
	<h4 class="entry_meta"><?php echo get_the_date(); ?> - <?php the_author_posts_link() ?></h4>
	<div class="entry_content">
		<?php the_excerpt(); ?>
	</div>
	<p class="button"><a href="<?php the_permalink(); ?>" class="button-red-stroked">Continue Reading</a></p>
</li>
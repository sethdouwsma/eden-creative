jQuery(document).ready(function ($) {

	// Header toggle
	$('.header_toggle').click(function() {
		$('.navigation').toggleClass( "toggled" );
	});

	var iOS = navigator.userAgent.match(/(iPod|iPhone|iPad)/);
	if(iOS){
	
	    function iosVhHeightBug() {
	        var height = $(window).height();
	        $("header").css('height', height);
	        $(".single-post header").css('height', (height / 1.8));
	        $(".about_footer").css('height', (height / 1.25));
	    }
	
	    iosVhHeightBug();
	    $( window ).resize(function() {
		  iosVhHeightBug();
		});
	}  
	
});

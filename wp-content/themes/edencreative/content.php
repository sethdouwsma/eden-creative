<?php
/**
 * The default template for displaying post content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry_header">
		<div class="author_image"><?php echo get_avatar(get_the_author_meta( 'ID' )); ?></div>
		<h3 class="entry_title"><?php the_title(); ?></h3>
		<h4 class="entry_meta"><?php echo get_the_date(); ?> - <?php the_author_posts_link() ?> - <?php the_category(', ') ?></h4>
		<hr>
	</div>
	<div class="entry_content">		
		<?php the_content(); ?>
	</div>
	
	<?php if ( in_category( 'the-why-initiative' ) && !($post->post_author == '1') && !($post->post_author == '2')  ) : ?>
	<div class="author_info clearfix">
		<div class="left_bucket">
			<div class="author_image"><?php echo get_avatar(get_the_author_meta( 'ID' )); ?></div>
		</div>
		<div class="right_bucket">
			<div class="author_description">
				<h3 class="author_title">About <?php the_author(); ?></h3>
				<p><?php the_author_meta('description'); ?></p>
				<p class="author_twitter"><a href="<?php the_author_meta('url'); ?>" target="_blank">Follow Me On Twitter</a></p>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
</article>
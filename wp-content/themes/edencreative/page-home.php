<?php 
/* 
Template Name: Home
*/
?>

<?php get_header(); ?>
	
	<header>
		<div class="container">
			<div class="logo">Eden Creative - Branding, UX/UI Design, and Development</div>
			<h1>Designing & Developing beautiful work to build <span>relationships</span> and make a <span>difference.</span></h1>
			<h2>Welcome to Eden.</h2>
		</div>	
	</header><!-- end header -->

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
				<div class="hp_about container">
					<div class="intro section">
						<h2>Eden is a team of strategic visionaries & creatives.</h2>
						<p>At Eden, we not only develop brands, we want to create a brand experience that customers will remember for a lifetime.</p>
					</div>
					
					<div class="buckets_wrap section">
						<ul class="service_buckets clearfix">
							<li class="bucket large-4 medium-12">
								<h3 class="branding">Branding</h3>
								<p>We love learning the ins and outs of our clients. By listening, learning, and researching, we position ourselves to effectively tell our client’s story through the use of graphics and content.</p>
								
								<p>A logo identifies a business in its simplest form and we will be sure to use all the tricks - from tailor-made fonts to eye-catching graphics, we explore every option to bring the brand experience full-circle.</p>
							</li>
							<li class="bucket large-4 medium-12">
								<h3 class="ui">UX/UI Design</h3>
								<p>Our designs not only look beautiful, they actually work. We consider every detail from user experience and accessibility to search engine optimization. A proven process creates proven results.</p>
								
								<p>We set out to design not only beautiful products, but ones that work and function seamlessly. The look and feel are as important as the experience, and at Eden, that's our focus.</p>
							</li>
							<li class="bucket large-4 medium-12">
								<h3 class="dev">Development</h3>
								<p>We believe in close collaboration between designers and developers. The result is a higher level of creativity using the right technologies for the job.</p>
								
								<p>We customize all solutions based on the needs of the client. We build custom Content Management Systems but work with off-the-shelf solutions if needed. Whatever system we deliver, we’ll make sure it fit’s perfectly.</p>
							</li>
						</ul>
						<p class="button"><a href="<?php echo get_page_link(11) ?>" class="button-red-stroked">Let's Work Together</a></p>
					</div>
				</div>
			
				<div class="section eden_fellas">
					<div class="container">
						<div class="large-6 medium-12">
							<h3>Who are these Eden fellas?</h3>
							<p>Born & raised in the Midwest, Eden Creative is a small, spirited team that helps startups and established companies plan, design and build brands. We believe proper planning lays a foundation for project success, so our focus is on execution and the important little details.</p>
							<p>We evaluate your ideas, projects, or businesses and problem solve through design to achieve a distinguished status which will guarantee separation from competitors and connect to your desired audience. By evaluating competition, modern trends, and demographics - we establish means for success. <em>We are listeners before speakers, we are thinkers before doers.</em></p>
							<p class="button"><a href="<?php echo get_page_link(5) ?>" class="button-red-stroked">Meet The Team</a></p>
						</div>	
					</div>	
				</div>
				
				<div class="cs_featured">
					<div class="intro section">
						<h2>Our Latest Work</h2>
						<p>A little preview into the great clients we get to work with on a daily basis. If you like what you see, go ahead and see how you can work with us. Still not satisfied?</p>
					</div>
					
					<ul class="cs_buckets">
						<?php
						 	$args = array(
					            'posts_per_page' => 3,
					            'post_type'	=> 'case_study',
							    'orderby'	=> 'date',
								'order'		=> 'DESC' //  Newst To Oldest
					        );
							query_posts( $args ); 
						?>
					  	<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'content', 'cs_excerpt' ); ?>
							<?php endwhile; ?>
					  	<?php endif; ?>
						<?php wp_reset_query(); ?>
					</ul>
				</div>
				
				<div class="dribbble">
					<div class="intro section">
						<img src="<?php echo get_template_directory_uri();?>/images/dribbble_gray.svg" alt="Dribbble - Eden Creative" />
						<h2>Work In Progress</h2>
						<p>Our most recent dribbble shots</p>
					</div>
					
					<script>
					dug({
					  endpoint: 'http://api.dribbble.com/players/edencreative/shots',
					  template: '<ul class="dribbble_wrap container clearfix">\
					    {{#shots|dug.limit:6}}\
					      <li>\
					        <a href="{{url}}" title="{{title}}" target="_blank">\
					          <img src="{{image_400_url}}" alt="Image of {{title}}">\
					        </a>\
					      </li>\
					    {{/shots|dug.limit:6}}\
					  </ul>'
					});
					</script>
				</div>
				
				<div class="journal">
					<div class="intro section">
						<h2>Guys Keep Journals Too...</h2>
						<p>Thoughts on all things branding, design, and development.</p>
					</div>
					<ul class="journal_wrap stacked container clearfix">
						<?php
						 	$args = array(
						        'posts_per_page' => 3,
							    'orderby'	=> 'date',
								'order'		=> 'DESC' //  Newst To Oldest
						    );
							query_posts( $args ); 
						?>
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'content', 'index_excerpt' ); ?>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
					</ul>
				</div>
				
				<div class="tweets_wrap">
					<div class="container">
						<img src="<?php echo get_template_directory_uri();?>/images/tweets_bg.svg" alt="Twitter - Eden Creative" />
						<div id="tweets"></div>
					</div>
				</div>
				
			</div><!-- end content -->
	
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>

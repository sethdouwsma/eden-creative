<?php 
/* 
Template Name: About
*/
?>

<?php get_header(); ?>
	
	<header>
		<div class="container">
			<h1>Work Hard, <span>Trust God</span>, Enjoy Life.</h1>
		</div>	
	</header><!-- end header -->

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
			
				<div class="container">
					<div class="about_intro intro section">
						<h2>Behind Every Dream is Passion</h2>
						<p>There was a belief inside of both of us that this could be done. The passion to serve others couldn’t be ignored and the strength of that belief pushed us to start Eden. As a young company, we use that same passion to propel us forward into the future. We put our clients first and push ourselves to not only do great work but to become better individuals as we embark on this journey.</p>
						<div class="quote">
							<h3>“Design can be art. Design can be aesthetics. Design is so simple, that's why it is so complicated.”</h3>
							<hr>
							<h5>- Paul Rand</h5>
						</div>
					</div>
					
					<div class="row section why">
						<div class="large-5 medium-12">
							<h3 class="red">‘Why’ Before ‘What’</h3>
							<p>This is the mindset we take on each day. A mindset in which that question comes first, results second and authenticity being the key to success. Not only do we desire to provide amazing design & development solutions, but also build incredible, lasting relationships. We believe that at the heart of everything, life is about positive impact and seeing others succeed. We want to see positive change and impact in our client circles.</p>
						</div>
						<div class="offset-1 large-5 medium-12">
							<h3 class="red">Executing the Plan</h3>
							<p>Executing the plan is pivotal to a successful project. At Eden, we evaluate your ideas, ask the right questions and problem solve through design to achieve a distinguished status which guarantees separation from competitors and connects to your desired audience. <em>1. Ask 2. Study 3. Understand 4. Execute.</em></p>
						</div>
					</div>
				</div>
				
				<div class="standards">
					<div class="container section">
						<div class="intro">
							<h2>Our Standards</h2>
							<p>What we live and work by at Eden</p>
						</div>
						<ol>
							<li class="medium-12">
								<h3>Work Hard, Trust God, Enjoy Life</h3>
								<p>Simple as that. For us, work is a means to make a difference in this life. We work hard at what we do, trust God and keep our purpose at the center of everything; and what the heck, let's have fun doing it!</p>
							</li>
							<li class="medium-12">
								<h3>Propelled by Passion</h3>
								<p>Keep the fire burning. Whether in a client meeting, working on wireframes, or at the tail end of a 3-month project, the passion will be there to keep the engines running at full capacity.</p>
							</li>
							<li class="medium-12">
								<h3>Always Improve</h3>
								<p>Our best now won’t be our best in the next project. We are continually and proactively positioning ourselves to improve through treading uncharted waters and reading new material; ultimately becoming masters of our trade.</p>
							</li>
							<li class="medium-12">
								<h3>All About the Experience</h3>
								<p>We want each client to have an incredible experience when working with Eden. We strive to develop great solutions while simultaneously developing authentic, long-lasting relationships.</p>
							</li>
							<li class="medium-12">
								<h3>Listen First</h3>
								<p>Our clients are our teammates. Without hearing their initial ideas, feedback throughout a project or final thoughts on the entire experience, we can’t provide an ample service. We listen first - all the time.</p>
							</li>
							<li class="medium-12">
								<h3>Begin with the End in Mind</h3>
								<p>We focus on the end product. The message it’s intended to send or the story we’re intending to tell. We only make moves when we can answer the question “why”. By beginning with the end in mind, we are giving ourselves a goal to reach and a race to win.</p>
							</li>
						</ol>
					</div>
				</div>
				
				<div class="bios_wrap section container">
					<h2>Our Two Man Band</h2>
					<ul class="bio_buckets">
						<li class="bio_bucket seth">
							<a href="<?php echo get_permalink(659); ?>">
								<div class="bio_bucket_image"></div>
								<h3>Seth Douwsma</h3>
								<h5>Partner, Designer, Developer</h5>
								<ul class="social_list bio_social">
									<li class="twitter"><a href="<?php the_field('twitter', 659); ?>" target="_blank">Twitter</a></li>
									<li class="dribbble"><a href="<?php the_field('dribbble', 659); ?>" target="_blank">Dribbble</a></li>
									<li class="linkedin"><a href="<?php the_field('linkedin', 659); ?>" target="_blank">LinkedIn</a></li>
									<li class="spotify"><a href="<?php the_field('spotify', 659); ?>" target="_blank">Spotify</a></li>
								</ul>
							</a>
						</li>	
						<li class="bio_bucket zach">
							<a href="<?php echo get_permalink(661); ?>">
								<div class="bio_bucket_image"></div>
								<h3>Zach Grantham</h3>
								<h5>Partner, Creative Director</h5>
								<ul class="social_list bio_social">
									<li class="twitter"><a href="<?php the_field('twitter', 661); ?>" target="_blank">Twitter</a></li>
									<li class="dribbble"><a href="<?php the_field('dribbble', 661); ?>" target="_blank">Dribbble</a></li>
									<li class="linkedin"><a href="<?php the_field('linkedin', 661); ?>" target="_blank">LinkedIn</a></li>
									<li class="spotify"><a href="<?php the_field('spotify', 661); ?>" target="_blank">Spotify</a></li>
								</ul>
							</a>
						</li>						
					</ul>
				</div>
				
				<div class="about_footer">
					<div class="container">
						<h1><span>Proudly</span> Made in the Midwest</h1>
					</div>	
				</div>								
			</div><!-- end content -->
	
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>

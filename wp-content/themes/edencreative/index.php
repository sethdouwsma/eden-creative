<?php get_header(); ?>
	
	<header>
		<div class="container">
			<h1>Our <span>Two</span> Cents</h1>
		</div>	
	</header><!-- end header -->

	<div class="content section container">
		<ul class="journal_wrap stacked">
			<?php
			 	$args = array(
		            'posts_per_page' => -1,
				    'orderby'	=> 'date',
					'order'		=> 'DESC' //  Newst To Oldest
		        );
				query_posts( $args ); 
			?>
		  	<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'index_excerpt' ); ?>
				<?php endwhile; ?>
		  	<?php endif; ?>
			<?php wp_reset_query(); ?>
		</ul>		
	</div><!-- end content -->


<?php get_footer(); ?>
<?php
/**
 * The default template for displaying case study excerpts
 *
 */
?>

<li class="cs_bucket" style="background-image: url(<?php the_field('bucket_featured_image'); ?>);">
	<a href="<?php the_permalink(); ?>">
		<div class="overlay"></div>
		<div class="container">
			<div class="large-6 medium-8 small-12">
				<h3><?php the_field('tagline'); ?></h3>
				<hr>
				<h4><?php the_title(); ?> - <?php the_field('location'); ?></h4>
				<p><a href="<?php the_permalink(); ?>" class="button-red-solid">View Case Study</a></p>
			</div>
		</div>
	</a>
</li>